import React from "react";

import Layout from "../components/layout";
import SEO from "../components/seo";

function IndexPage() {
  return (
    <Layout>
      <SEO
        title="Financial Delegation Widget"
      />

      <section className="text-center">

        <h2 className="inline-block p-1 mb-1 text-xl font-bold">
          Financial Delegations
        </h2>

        <div className="flex md:mx-20">
          <p className="leading-loose font-medium w-1/2">
            MPS
          </p>
          {/* <div className="flex w-10 items-center">
            <hr className = "flex-1 border-solid border border-gray-600" />
          </div> */}
          <p className="leading-loose w-1/2">
            $0 - $1,999
          </p>
        </div>
        <div className="flex md:mx-20">
          <p className="leading-loose font-medium w-1/2">
            Manager
          </p>
          {/* <div className="flex w-10 items-center">
            <hr className = "flex-1 border-solid border border-gray-600" />
          </div> */}
          <p className="leading-loose w-1/2">
            $2,000 - $9,999
          </p>
        </div>
        <div className="flex md:mx-20">
          <p className="leading-loose font-medium w-1/2">
            DSPP
          </p>
          {/* <div className="flex w-10 items-center">
            <hr className = "flex-1 border-solid border border-gray-600" />
          </div> */}
          <p className="leading-loose w-1/2">
            $10,000 - $49,999
          </p>
        </div>
        <div className="flex md:mx-20">
          <p className="leading-loose font-medium w-1/2">
            SPP
          </p>
          {/* <div className="flex w-10 items-center">
            <hr className = "flex-1 border-solid border border-gray-600" />
          </div> */}
          <p className="leading-loose w-1/2">
            $50,000 - $1,200,000
          </p>
        </div>
      </section>
    </Layout>
  );
}

export default IndexPage;

// Role,Limit
// MPS,2000
// Manager,10000
// DSPP,50000
// SPP,1200000